import { Component, OnInit, Input } from '@angular/core';
import * as menuOption from '../../../resources/menu.json';
import { EthcontractService } from 'src/app/services/ethcontract.service';
import { ColorService } from 'src/app/services/color.service';
import { account } from 'src/model/account.model';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit{

  isExpanded = false;
  menu: any = (menuOption as any).default.menu;
  account: string="";
  balance: string = "";
  themeColor: string = "primary";
  constructor(private contractService: EthcontractService, private color: ColorService) {


  }

  // 
  

  ngOnInit(){
    this.contractService.getAccountInfo().subscribe(data=>{
      data.forEach(datos=>{
        this.account = datos[0];
        this.balance = (datos[1].c[1]);
      })
    });
    this.color.color.subscribe(data=>{
      this.themeColor = data;
    });
  }
}
