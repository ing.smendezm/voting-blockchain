import { Injectable } from '@angular/core';
import * as Web3 from 'web3';
import * as TruffleContract from 'truffle-contract';
import { voting } from 'src/model/voting.model';
import { Observable, BehaviorSubject } from 'rxjs';

declare let require: any;
declare let window: any;

let token = require('../../../../build/contracts/Election.json');

@Injectable({
  providedIn: 'root'
})
export class EthcontractService {

  private web3Provider: null;
  private contracts:{};
  public bool = new BehaviorSubject<Boolean>(false);

  constructor() {

    if(typeof window.web3 !== 'undefined' ){
      this.web3Provider = window.web3.currentProvider;
    }else{
      this.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:7545');
    }
    window.web3 = new Web3(this.web3Provider);



   }

   getAccountInfo(): Observable<any[]> {

     var a = new BehaviorSubject<any[]>([]);
     let current = a.value;
     
     new Promise((resolve, reject) => {
      window.web3.eth.getCoinbase(function(err, account) {
        if(err === null) {
          window.web3.eth.getBalance(account, function(err, balance){ 
        if(err === null) {
          current.push([account,window.web3.fromWei(balance, 'ether')]);
        } else {
          current.push(["Error", "Error"]);
          } 
          a.next(current);
          });
        }
      });
      
    });
    return a;
  }

   loadDataContract(): Observable<any[]>{
    let contract = TruffleContract(token);
    contract.setProvider(this.web3Provider);
    let electionInstance;

    let data: Object[] = [];
    let datas = new BehaviorSubject([]);

    contract.deployed().then(instance=>{
      
      electionInstance = instance;

      return electionInstance.candidatesCount();
    }).then(candidatesCount=>{
      for(var i = 1; i <= candidatesCount; i++){
        electionInstance.candidates(i).then(candidate=>{
          var id = candidate[0].words[0];
          var name = candidate[1];
          var voteCount = candidate[2].words[0];;
          let dato =  <voting>{"id": id, "name": name, "votos":voteCount };
          data.push(dato);
          datas.next(data);
        });
        }
        return electionInstance.voters(window.web3.eth.accounts[0]);
    }).then(hasVoted=>{
      console.log(hasVoted);
      if(hasVoted){
  
        this.bool.next(true);
      }
    })
    .catch(err=>{
      console.log(err);
    });
    return datas;
  }

  vote(id:string){
    let contract = TruffleContract(token);
    contract.setProvider(this.web3Provider);
    let electionInstance;

    contract.deployed().then(instance=>{
      electionInstance = instance;
      return electionInstance.vote(id, {from: window.web3.eth.accounts[0]});
  }).then(resul=>{
    console.log(resul)
  }).catch(err=>{
    console.log(err);
  });

}

returnBool(): Observable<Boolean>{

  return this.bool;
}

}
