import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ColorService {

  color = new BehaviorSubject<string>("primary");

  constructor() { }

  changeColor(color:string){
    this.color.next(color);
  }

}
