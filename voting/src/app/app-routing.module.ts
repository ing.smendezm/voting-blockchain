import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { SidenavComponent } from './component/sidenav/sidenav.component';
import { IndicatorsComponent } from './pages/indicators/indicators.component';
import { SettingComponent } from './pages/setting/setting.component';
import { RegisterComponent } from './pages/register/register.component';


const routes: Routes = [{path: '', redirectTo:'login', pathMatch: 'full'},
                        {path: 'login', component: LoginComponent},
                        {path: 'register', component: RegisterComponent },
                        {path: 'home', component: SidenavComponent, children:[
                          { path: 'principal', component: HomeComponent },
                          { path: 'indicators', component: IndicatorsComponent },
                          { path: 'settings', component: SettingComponent } ]
                      }
                      ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
