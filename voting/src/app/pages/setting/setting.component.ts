import { Component, OnInit } from '@angular/core';
import { ColorService } from 'src/app/services/color.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {

  
  constructor(private colorService: ColorService) { }
  Themes: any[] = ["warn", "accent", "primary"];
  ngOnInit() {
    
  }
  data($event){
    this.colorService.changeColor($event.value);
    console.log($event.value);

  }
}
