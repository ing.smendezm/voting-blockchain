import { Component, OnInit } from '@angular/core';
import { EthcontractService } from 'src/app/services/ethcontract.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  name: string;
  candidates: any;
  show: Boolean;
  constructor(private contractService: EthcontractService) { }

  ngOnInit() {
    this.contractService.loadDataContract().subscribe(data=>{
      this.candidates = data;
    });
    // this.contractService.returnBool().subscribe(data=>{
    //   this.show = data;
    // });
    
  
  }
  
  vote(id: string){
    this.contractService.vote(id);
    ///window.location.reload();
  }

}
