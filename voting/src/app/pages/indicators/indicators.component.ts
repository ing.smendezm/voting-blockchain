import { Component, OnInit } from '@angular/core';

import { LegendLabelsContentArgs } from '@progress/kendo-angular-charts';
import { EthcontractService } from 'src/app/services/ethcontract.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-indicators',
  templateUrl: './indicators.component.html',
  styleUrls: ['./indicators.component.scss']
})
export class IndicatorsComponent implements OnInit {

  data: Observable<any[]>;
  pieData: any[] = [];

  constructor(private eth: EthcontractService) { 

    
  }

  ngOnInit() {
    
    this.eth.loadDataContract().subscribe(data=>{
      this.pieData = data;
      this.labelContent = this.labelContent.bind(this);
    });
    
  }

  public labelContent(args: LegendLabelsContentArgs): string {
    return `${args.dataItem.name} : ${args.dataItem.votos} votos`;
}
}
