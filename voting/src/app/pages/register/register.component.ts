import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  tryFields(){
    return (this.userFormControl.invalid || this.passwordFormControl.invalid) ? false : true;
  }
  
  cleanFields(){
    this.userFormControl.setValue("");
    this.passwordFormControl.setValue("");
    this.emailFormControl.setValue("");
  }

  testUser(){
    if(this.tryFields()){

        //Servicio de Comprobacion de Usuario(Web Service)
        //this.cleanFields(); Limpia los Campos
        console.log("Funcionalidad en Construccion")
    } else{
      alert("Por Favor Ingrese toda la informacion Solicitada");
    }
  }

  userFormControl = new FormControl('', [
    Validators.required
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required
  ]);
  emailFormControl = new FormControl('',[
    Validators.required,
    Validators.email
  ])
}
