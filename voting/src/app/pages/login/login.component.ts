import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  tryFields(){
    return (this.userFormControl.invalid || this.passwordFormControl.invalid) ? false : true;
  }
  
  cleanFields(){
    this.userFormControl.setValue("");
    this.passwordFormControl.setValue("");
  }

  testUser(){
    if(this.tryFields()){

        //Servicio de Comprobacion de Usuario(Web Service)
        //this.cleanFields(); Limpia los Campos
        if(this.userFormControl.value == "test" && this.passwordFormControl.value =="test"){
          this.router.navigate(['home/principal']);
        }
    } else{
      alert("Por Favor Ingrese toda la informacion Solicitada");
    }
  }

  userFormControl = new FormControl('', [
    Validators.required
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required
  ]);
}
