pragma solidity >=0.4.21 <0.7.0;

contract Election {
    string public candidate;

    //Modelo para los candidatos
    struct Candidate {
        uint id;
        string name;
        uint voteCount;
    }

    //Donde vamos a guardar todos esos candidatos
    mapping (uint=>Candidate) public candidates;
    //Donde guardaremos a los votantes
    mapping (address=>bool) public voters;
    //Esta variable es para saber cuantos candidatos hay
    uint public candidatesCount;

    constructor() public{
        addCandidate("Sebastian");
        addCandidate("Erick");
        addCandidate("Eddy");
        addCandidate("Juanito");
        addCandidate("Guillo");
    }

    //Funciones de la logica
    function addCandidate(string memory _name) private{
        candidatesCount++;
        candidates[candidatesCount] = Candidate(candidatesCount,_name,0);
    }

    function vote(uint _candidate) public {
        //que no haya votado antes
        // require(!voters[msg.sender]);
        // //que sea valido
        // require(_candidate>0);
        // //se guarda el voto
         voters[msg.sender] = true;
        //se actualiza la cuenta de los votos
        candidates[_candidate].voteCount++;
    }
}